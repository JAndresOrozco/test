//
//  RegisterView.swift
//  Test
//
//  Created by Solidusystems on 04/05/21.
//

import SwiftUI

struct RegisterView: View {
    
    @State private var username: String = ""
    @State private var password: String = ""
    @State private var isRegister: Bool = false
    @State private var isLogin: Bool = false
    @ObservedObject var user = User()
    
    var body: some View {
        ZStack {
            Color.black
                .edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
            
            VStack(alignment: .center, spacing: 15) {
                AppLogo()
                Text("Registrarse")
                    .foregroundColor(.white)
                    .font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)
                Text("Ingresa tus datos")
                    .foregroundColor(.white)
                    .font(.subheadline)
                    .padding(.bottom, 10)
                TextField("Usuario", text: $username)
                    .padding()
                    .foregroundColor(.black)
                    .background(Color("themeTextField"))
                    .cornerRadius(10.0)
                SecureField("Contraseña", text: $password)
                    .padding()
                    .foregroundColor(.black)
                    .background(Color("themeTextField"))
                    .cornerRadius(10.0)
                Button(action:{
                    UserDefaults.standard.set(username, forKey: "username")
                    UserDefaults.standard.set(password, forKey: "password")
                    self.isRegister = true
                    print($username, $password)
                }, label: {
                    Text("REGISTRARSE")
                        .bold()
                        .padding()
                        .frame(width: 220, height: 50)
                        .foregroundColor(.white)
                        .background(Color.red)
                        .cornerRadius(10.0)
                }).alert(isPresented: $isRegister, content: {
                    Alert(title: Text("Éxito"), message: Text("Ya estás registrado"), dismissButton: .default(Text("Aceptar")))
                })
                Button(action: {
                    self.isLogin = true
                }, label: {
                    Text("¿Ya tienes cuenta? Inicia Sesión")
                        .padding()
                        .foregroundColor(.white)
                        .cornerRadius(10.0)
                        .font(.body)

                })
            }.padding(35)
            
            if isLogin{
                LoginView()
            }
        }
    }
}

struct RegisterView_Previews: PreviewProvider {
    static var previews: some View {
        RegisterView()
    }
}
