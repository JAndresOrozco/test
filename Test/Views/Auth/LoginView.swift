//
//  LoginView.swift
//  Test
//
//  Created by Solidusystems on 04/05/21.
//

import SwiftUI

struct LoginView: View {
    @State private var username: String = ""
    @State private var password: String = ""
    @State private var isLoginValid: Bool = false
    @State private var IsLogin: Bool = false
    @State private var IsRegister: Bool = false
    @ObservedObject var user = User()
    
    var body: some View {
        ZStack {
            Color.black
                .edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
            VStack(alignment: .center, spacing: 15) {
                AppLogo()
                Text("Iniciar Sesión")
                    .foregroundColor(.white)
                    .font(.title)
                    .bold()
                    .padding()
                TextField("Usuario", text: $username)
                    .padding()
                    .background(Color("themeTextField"))
                    .cornerRadius(10.0)
                
                SecureField("Contraseña", text: $password)
                    .padding()
                    .background(Color("themeTextField"))
                    .cornerRadius(10.0)
                
                Button( action: {
                    let isLoginValid = self.username == self.user.username && self.password == self.user.password
                    if isLoginValid {
                        self.isLoginValid = true
                        self.IsLogin = true
                    }
                    
                    print($user.password, $user.username)
                   
                }, label: {
                    Text("INICIAR SESIÓN")
                        .bold()
                        .padding()
                        .frame(width: 220, height: 50)
                        .foregroundColor(.white)
                        .background(Color.red)
                        .cornerRadius(10.0)
                }).alert(isPresented: $isLoginValid, content: {
                    Alert(title: Text("Éxito"), message: Text("Bienvenido"), dismissButton: .default(Text("Aceptar")))
                })
                Button(action: {
                    IsRegister = true
                }, label: {
                    Text("¿Aún no tienes cuenta? Registrate")
                        .padding()
                        .foregroundColor(.white)
                        .cornerRadius(10.0)
                        .font(.body)

                })
            }.padding(35)
            
            if IsRegister {
                RegisterView()
            }
            
            if IsLogin {
                HomeView()
            }
            
        }
        
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}

struct AppLogo : View {
    var body: some View {
        return Image("appLogo")
            .resizable()
            .aspectRatio(UIImage(named: "appLogo")!.size, contentMode: .fill)
            .frame(width: 100, height: 100)
            .clipped()
            .cornerRadius(150)
            .shadow(color: .red, radius: 5, x: /*@START_MENU_TOKEN@*/0.0/*@END_MENU_TOKEN@*/, y: /*@START_MENU_TOKEN@*/0.0/*@END_MENU_TOKEN@*/)
            .padding(.bottom, 0)
    }
}


