//
//  User.swift
//  Test
//
//  Created by Solidusystems on 04/05/21.
//

import Foundation

class User: ObservableObject{
    @Published var username: String {
        didSet {
            UserDefaults.standard.set(username, forKey: "username")
        }
    }
    @Published var password: String {
        didSet {
            UserDefaults.standard.set(password, forKey: "password")
        }
    }
    
    init() {
        username = UserDefaults.standard.string(forKey: "username") ?? ""
        password = UserDefaults.standard.string(forKey: "password") ?? ""
    }
    
}
