//
//  SplashView.swift
//  Test
//
//  Created by Solidusystems on 05/05/21.
//

import SwiftUI

struct SplashView: View {
    @State private var isActive: Bool = false;
    var body: some View {
        ZStack {
            Color.black
                .edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
            VStack(alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/, spacing: /*@START_MENU_TOKEN@*/nil/*@END_MENU_TOKEN@*/, content: {
                if self.isActive {
                    LoginView()
                } else {
                    Image("appLogo")
                        .resizable()
                        .aspectRatio(UIImage(named: "appLogo")!.size, contentMode: .fill)
                        .frame(width: 100, height: 100)
                        .clipped()
                        .cornerRadius(150)
                        .shadow(color: .red, radius: 5, x: /*@START_MENU_TOKEN@*/0.0/*@END_MENU_TOKEN@*/, y: /*@START_MENU_TOKEN@*/0.0/*@END_MENU_TOKEN@*/)
                        .padding(.bottom, 0)
                }
                
            }).onAppear() {
                DispatchQueue.main.asyncAfter(deadline: .now() + 2.5){
                    withAnimation {
                        self.isActive = true
                    }
                }
            }
        }
    }
}

struct SplashView_Previews: PreviewProvider {
    static var previews: some View {
        SplashView()
    }
}
